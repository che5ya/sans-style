# Sans-Style

A set of small, minimal, responsive CSS Module for every web project.
The name "Sans-Style" was inspired by the game undertail "sans the 'skeleton'"(Provisional designation name).


## Features

- A responsive flex based grid that can be customized to needs.
- Pre-Defined CSS style elements that **NOT AFFECT THE DESIGNER'S WORKS**. (Final Goal)
- Initialize User Agent Stylesheet based on Normalize.css and Eric Meyer's Reset CSS.
- Consistently styled elements that work with a and button tags.


## Get Started

* TBA

```bash
$ npm install sans-style
```


## Usage

* TBA


## Author

**sans-style** © [CHE5YA](https://github.com/che5ya), Released under the [MIT](./LICENSE) License.<br>

> GitHub [@che5ya](https://github.com/che5ya) · Facebook [@che5ya](https://www.facebook.com/che5ya) · Twitch [@chesya_](https://www.twitch.tv/chesya_)


## License

[MIT](LICENSE)
